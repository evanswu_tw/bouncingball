package ball.model;

public class BallFactory {

    public static Ball[] all() {
        return new Ball[]{
                bouncingBall(75, 50, BouncingBehavior.DOWN),
                elasticBall(250, 100, Ball.DEFAULT_RADIUS, ElasticBehavior.SHRINK),
                elasticBouncingBall(400,200,Ball.DEFAULT_RADIUS,BouncingBehavior.DOWN,ElasticBehavior.SHRINK)
        };
    }

    public static Ball bouncingBall(int centerX, int centerY, int direction) {

        Ball ball = new Ball(centerX, centerY, Ball.DEFAULT_RADIUS);
        ball.addBehavior(new BouncingBehavior(ball, direction));
        return ball;
    }

    public static Ball elasticBall(int centerX, int centerY, int radius, int direction) {
        Ball ball = new Ball(centerX, centerY, radius);
        ball.addBehavior(new ElasticBehavior(ball, direction));
        return ball;
    }


    public static Ball elasticBouncingBall(int centerX, int centerY, int radius, int boucingDirection, int elasticDirection) {
        Ball ball = new Ball(centerX, centerY, radius);
        ElasticBehavior elasticBehavior = new ElasticBehavior(ball, elasticDirection);
        BouncingBehavior bouncingBehavior = new BouncingBehavior(ball, boucingDirection);

        ball.addBehavior(elasticBehavior);
        ball.addBehavior(bouncingBehavior);
        return ball;
    }
}
