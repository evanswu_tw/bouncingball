package ball.model;

public interface Behavior {
    void change();
}
