package ball.model;

public class ElasticBehavior implements Behavior {

    public static final int GROWTH_RATE = 2;


    static final int GROW = 1;
    static final int SHRINK = -1;
    private final Ball ball;

    private int growthDirection;
    private Behavior[] behaviors;



    public ElasticBehavior(Ball ball, int direction) {
        this.ball = ball;

        growthDirection = direction;

    }

    @Override
    public void change(){
        growthDirection = reverseGrowthDirectionIfNecessary();
        ball.setRadius(next());
    }

    /***********************************************************************************
     *
     * Do not change Elastic ALGORITHM below.
     *
     ***********************************************************************************/

    private int reverseGrowthDirectionIfNecessary() {
        if (growingTooBig() || shrinkingTooSmall()) {
            return switchDirection();
        }
        return this.growthDirection;
    }

    private boolean shrinkingTooSmall() {
        return ball.getRadius() <= 0 && shrinking();
    }

    private boolean growingTooBig() {
        return ball.getRadius() >= Ball.DEFAULT_RADIUS && growing();
    }

    private int switchDirection() {
        return growing() ? SHRINK : GROW;
    }

    private int next() {
        return ball.getRadius() + (GROWTH_RATE * growthDirection);
    }

    private boolean shrinking() {
        return growthDirection == SHRINK;
    }

    private boolean growing() {
        return growthDirection == GROW;
    }
}
