package ball.model;

import java.awt.*;
import java.util.ArrayList;

public class Ball {
    protected static final int DEFAULT_RADIUS = 50;
    protected int x;
    protected int y;
    protected int radius;
    // make sure we define the generic and 
    // use List instead of ArrayList 
    protected ArrayList<Behavior> behaviors = new ArrayList<>();

    protected Ball(int x, int y, int radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    protected Ball(int x, int y) {
        this(x, y, DEFAULT_RADIUS);
    }

    // DO NOT CHANGE
    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius){
        this.radius = radius;
    }

    // DO NOT CHANGE
    public Point center() {
        return new Point(x, y);
    }

    public void addBehavior(Behavior behavior) {
        behaviors.add(behavior);
    }

    public void update() {
        for (Behavior behavior: behaviors){
            behavior.change();
        }
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
