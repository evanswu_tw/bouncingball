package ball.model;

import ball.ui.BallWorld;


public class BouncingBehavior implements Behavior {

    public static final int MOVEMENT_SPEED = 12;

    protected  Ball ball;
    static final int DOWN = 1;
    static final int UP = -1;

    private int direction;

    BouncingBehavior(Ball ball, int direction){
        this.ball = ball;
        this.direction = direction;
    }


    @Override
    public void change() {
        direction = reverseDirectionIfNecessary();
        ball.setY(move());
    }

    /***********************************************************************************
     *
     * Do not change Bouncing ALGORITHM below.
     *
     ***********************************************************************************/

    private int reverseDirectionIfNecessary() {
        if (movingTooHigh() || movingTooLow()) {
            return switchDirection();
        }

        return this.direction;
    }

    //TODO: change the getY name more meaningful;
    private boolean movingTooLow() {
        return ball.getY() + ball.radius >= BallWorld.BOX_HEIGHT && movingDown();
    }

    private boolean movingTooHigh() {
        return ball.getY() - ball.radius <= 0 && movingUp();
    }

    private int switchDirection() {
        return movingDown() ? UP : DOWN;
    }

    private int move() {
        return ball.getY() + (MOVEMENT_SPEED * direction);
    }

    private boolean movingDown() {
        return direction == DOWN;
    }

    private boolean movingUp() {
        return direction == UP;
    }




}
