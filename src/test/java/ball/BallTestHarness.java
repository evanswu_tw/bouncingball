package ball;

import ball.model.Ball;
import ball.model.BouncingBehavior;
import ball.model.ElasticBehavior;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BallTestHarness {
    public static void assertCenterYCoordinateIs(int expectedCenterY, Ball ball) {
        assertEquals(expectedCenterY, ball.center().y, "Ball is at the wrong y-coordinate!");
    }

    public static int oneStepDownFrom(int centerY) {
        return centerY + BouncingBehavior.MOVEMENT_SPEED;
    }

    public static int oneStepUpFrom(int centerY) {
        return centerY - BouncingBehavior.MOVEMENT_SPEED;
    }


    public static void assertRadiusIs(int expectedRadius, Ball ball) {
        assertEquals(expectedRadius, ball.getRadius(), "Ball does not have the correct getRadius");
    }

    public static int oneStepInwardsFrom(int radius) {
        return radius - ElasticBehavior.GROWTH_RATE;
    }

    public static int oneStepOutwardsFrom(int radius) {
        return radius + ElasticBehavior.GROWTH_RATE;
    }
}