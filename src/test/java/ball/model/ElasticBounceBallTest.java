package ball.model;

import org.junit.jupiter.api.Test;

import static ball.BallTestHarness.*;

public class ElasticBounceBallTest {

    @Test
    public void shouldGoDown() throws Exception {
        Ball elasticBouncingBall = BallFactory.elasticBouncingBall(0, 100, 20, BouncingBehavior.DOWN, ElasticBehavior.GROW);
        elasticBouncingBall.update();
        assertCenterYCoordinateIs(oneStepDownFrom(100), elasticBouncingBall);
    }

    @Test
    public void shouldIncreaseRadius() throws Exception {
        Ball elasticBouncingBall = BallFactory.elasticBouncingBall(0, 100, 20, BouncingBehavior.DOWN, ElasticBehavior.GROW);
        elasticBouncingBall.update();
        assertRadiusIs(oneStepOutwardsFrom(20), elasticBouncingBall);
    }
}
